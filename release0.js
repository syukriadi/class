class Animal {
    // Code class di sini
    constructor(name){
        this.legs=2
        super(name);
    }
    yell(){
        return "Auooo"
    }
}

class frog extends Animal(){
    constructor(name){
        super(name);
    }
   jump(){
       console.log("hop hop")
   } 
}
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false