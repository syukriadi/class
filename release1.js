// Code class Ape dan class Frog di sini
class Frog extends Animal(){
    constructor(name){
        super(name);
    }
  jump(){
      console.log("hop hop")
  }  
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
